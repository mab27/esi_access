# ESI LAG at Access Layer - Configuration Requirements

AIS 4.0.1 and above supports Access swiches as part of a Blueprint. It allows using lower cost access switches to aggregate server ports for an EVPN capable leaf. Access layer also allows aggregating lower speed (1G/10G) server ports towards a higher capacity leaf.  

However dual-homing towards the server facing ports is currently not supported with access switches. We will highlight the configuration requirements on access switches to support ESI-LAG for dual-homing towards the server ports.  

For this requirement, we will leverage existing functionality in AIS for EVPN Leafs (Collpased Fabirc (Spine-less) Model) and bring in relevent portion of the EVPN Leaf configuration.  

To reuse what is already available in AIS 4.0.1, we will highlight relevent portion and diff between the two configuration files, rendered for the topology below.

![Apstra ESI Leaf Access Topology](./esi_leaf_access_topology.jpg)

1. [**evpn_leaf_junos.config**](./evpn_leaf_junos.config) - Junos Configuration rendered in AIS 4.0.1 for a collapsed EVPN Leaf (_leaf1_)
2. [**l2_access_junos.config**](./l2_access_junos.config) - Junos Configuration rendered in AIS 4.0.1 for a collapsed EVPN Leaf (_access1_)

Please note that the exisiting rendered configuration does not support the required ESI-LAG functionality. This document is attempting to reuse available configuration blocks in EVPN_Leaf configuration and add relevant portion to the L2_Acesss configuration.

Lets consider rendered L2 Access configuration *l2_access_junos.config* as our base config.

Configuration to be added is prefixed with a "+"
Configuration to be removed is prefixed with a "-"

## 1. Direct IP Link between a Pair of Access Switches (Access_Cross_Link)

Just like the leaf-leaf mesh link between evpn-leaf switches in collpased fabric model, we would need an IP link between each pair of access switches where the servers can host their dual-home connections.

This *Access_Cross_Link* will be used for 
1. IP Connectivity between Access switches
2. It will carry EBGP Underlay Session to exchange loopback address configured on access switches
3. EBGP session for Overlay to be established between *lo0* IP addresses on the access switches. Overlay BGP session will exchange ESI_LAG related evpn updates between the two access switches
4. In case of the Uplink failure on the access switch, this *Access_Cross_Link* can also carry traffic towards the access switch that has fabric connectivity

For *Access_Cross_Link* redundnancy a user may decide to use an aggregate bundle. In that case they will designate an "aggregate/ae" interface for this purpose. 

### 1.1 Single Interface for *Access_Cross_Link*
Lets look at an example where a single inteface is used as *Access_Cross_Link*.

```diff 
 interfaces {  
     replace: xe-0/0/0 {  
+        description "facing_access2:xe-0/0/0";  
+        mtu 9216;  
         unit 0 {  
-           family inet;  
+            family inet {  
+                address 192.168.0.7/31;  
+            }  
         }  
     }  
```

### 1.2 Aggregate Interface for *Access_Cross_Link*
We propose using/reserving aggregate bundle **ae0** for *Access_Cross_Link*. Also make sure to count this bundle in count for chassis configuration.

TBD - Config example

## 2. Fabric (Leafs) Facing L2 Link on Access Switches

A typical Access switch will have redundant connections to a pair of EVPN Leaf Switches. And redundant links are prvisioned in an aggregate bundle. This is currently supported in Access swith configuration.

However to avoid forwarding loops in this topology, we need to modify how aggregate bundles will be paired.

### AIS 4.0.1 Access switch to Fabric Configuration

Currently, AIS configures aggregating Leaf switches with an ESI-LAG-id per access switch. It means one bundle spanning 2xEVPN-Leaf switches, and access swiches see both EVPN-Leafs as a single System.

However on the access switch side, the bundle is local to the switch and does not span to the paired access switch. And each access-switch acts as an independent switch for BUM traffic blocking.

### Fabric Interface Configuration to support Access ESI-LAG

To avoid BUM traffic forwarding loops,

1. Access Switches - Configure Fabric (Leaf-Pair) facing ports in ESI-LAG bundle spanning access-switch pair in the rack. It means you will configure same ESI-Id and LACP-Id facing both Leaf switches, on both access switches.
2. EVPN Leaf Switches - Configure Access switches facing ports for both access-switches in a single ESI-LAG bundle spanning leaf-switch pair in the rack. It means you will configure both Leaf switches with same ESI-Id and LACP-Id facing both access switches.

 Now the pair of access-switches will appear as single system for the Leafs. So overall it would be like one access system and one Leaf system. It will allow ESI/EVPN signaling mechanism to block BUM traffic loops and using DF election mechanism. 

### 2.1 Access-Switch - Fabric facing port configuration 

#### Access1 access-switch configuration

```diff 
     replace: xe-0/0/1 {  
+        description "facing_access1-leaf1:xe-0/0/1";  
+         ether-options {  
+             802.3ad ae1;  
         }  
     }  
     replace: xe-0/0/2 {  
+        description "facing_facing_access1-leaf2:xe-0/0/1:xe-0/0/1";  
+         ether-options {  
+            802.3ad ae1;  
         }  
     }  
     replace: ae1 {  
+        description "facing_leaf-pair1";  
+        esi {  
+            00:02:00:00:00:00:02:00:00:01;  
+            all-active;  
+        }  
         aggregated-ether-options {  
             lacp {  
                 active;  
+                system-id 02:00:00:00:00:01;  
             }  
         }  
         mtu 9100;  
         }  
     }  
```
Notice above we are using the same bundle *"ae1"* and ESI-Id towards both Leaf Switches Leaf1 and Leaf2.

Access-switch access2 will include a similar configuration, matching aggregate bundle number, esi-id and lacp-id.

### 2.1 Leaf-Switch - Access facing port configuration

As this section focuses on Access-switch configuration, we will include Leaf configuration in the relevant section x.x.x

## 3. Access-Switch - Server facing port configuration

Servers in a rack can be single-homed or dual-homed to the top of rack access-switches. Single-homed server configuration does not change from existing model, here we will focus on dual-homed server configuration.

### 3.1 Dual-Home Server facing port configuration

It is a typical ESI-LAG configuration facing dual-homed server. ESI-Id, lacp-id will be same on both access-switches for a given server and preferably the aggregate bundle should match as well.

```diff
     replace: xe-0/0/3 {  
+        description "to.sys001";  
         ether-options {  
+            802.3ad ae2;  
         }  
     }  
     replace: xe-0/0/4 {  
+        description "to.sys002";  
         ether-options {  
+            802.3ad ae3;  
         }  
     }  
     replace: ae2 {  
+        description "to.sys001";  
+        esi {  
+            00:02:00:00:00:00:01:00:00:02;  
+            all-active;  
+        }  
+        aggregated-ether-options {  
+            lacp {  
+                active;  
+                system-id 02:00:00:00:00:02;  
+            }  
+        }  
        mtu 9100;  
        unit 0 {  
            family ethernet-switching {  
                interface-mode trunk  
                vlan {  
                    members [  
                        vn3  
                    ]  
                 }  
             }  
         }  
     }
 vlans {  
     vn3 {  
         vlan-id 3;  
         description "VN_Blue";  
+        vxlan {  
+            vni 10005;  
+        }  
-        l3-interface irb.3;  
     }  
 }  
```

Notice other than the ESI-LAG related additions, other configuration on the server facing interface will be following existing model, but there are a couple of excpetions.

We would need vxlan vni information like evpn leaf, but currently it is not configured in existing access-switch model.

Also calling out the diff to remove irb.x assignement in traditional evpn leaf.

Similar configuration will be used for other servers. Also note that we will need to support other existing interface configurations.

### 3.2. Single home server configuration
It will use existing configuration model. 

## 3. Access-Switch - loopback interface configuration

As discussed earlier in section 1, we will need IP address configuration on loopback address to allow overlay BGP peering.

```diff
+    lo0 {  
+        unit 0 {  
+            family inet {  
+                address 10.0.0.27/32;  
+            }  
+        }  
-        unit 2 {  
-            family inet {  
-                address 172.16.2.0/32;  
-            }  
-        }  
    }  
-    irb {  
-        unit 3 {  
-            mac 00:1c:73:00:00:01;  
-            family inet {  
-                mtu 9000;  
-                address 172.16.1.1/24;  
-            }  
-        }  
-    }  
}  
```

Please note. we are specifically calling out a diff from traditional evpn-leaf configuration. As access switches will only be supporting L2 trunking towards the fabric, we will not configure per-vni loopbacks, and no IRB interface configuration.

## 4. Access-Switch - NO Routing-instance

As we are only using EVPN Signaling for ESI-LAGs, we can remove any VRF (Routing-Zone Configuration), on access switches. However please note that we would need to include VNI/VXLAN information for evpn signaling.

```diff

-routing-instances {  
-    Blue {  
-        instance-type vrf;  
-        interface irb.3;  
-        interface lo0.2;  
-        route-distinguisher 10.0.0.27:2;  
-        vrf-target target:10004:1;  
-        vrf-table-label;  
-        routing-options {  
-            multipath;  
-            auto-export;  
-        }  
-        protocols {  
-            evpn {  
-                ip-prefix-routes {  
-                    advertise direct-nexthop;  
-                    encapsulation vxlan;  
-                    vni 10004;  
-                    export BGP-AOS-Policy;  
-                }  
-            }  
-        }  
-    }  
-}  
```

## 5. Routing-Options, Switch-Options and other Protocols configuration

We will continue to use these sections similar to traditional Leaf configuration.

```diff
 routing-options {  
     router-id 10.0.0.27;  
     autonomous-system 64520;  
     forwarding-table {  
         export PFE-LB;  
         ecmp-fast-reroute;  
         chained-composite-next-hop {  
             ingress {  
                 evpn;  
             }  
         }  
     }  
 }  
  switch-options {  
     vtep-source-interface lo0.0;  
     route-distinguisher 10.0.0.27:65534;  
     vrf-target target:100:100;  
 }  
  protocols {  
     lldp {  
         port-id-subtype interface-name;  
         port-description-type interface-description;  
         interface all;  
     }  
     replace: rstp {  
         interface ae2 {  
             edge;  
         }  
         bridge-priority 0;  
         interface ae3;  
         interface ae4;  
         bpdu-block-on-edge;  
     }  
  }

```

### 5.1 Protocols EVPN Configuration

For most part, it will also be the same as traditional Leaf configuration, except one addition.

``` diff
  protocols { 
     evpn {  
         vni-options {  
             vni 10005 {  
                 vrf-target target:10005:1;  
             }  
         }  
         encapsulation vxlan;  
         default-gateway do-not-advertise;  
         extended-vni-list all;  
+        no-core-isolation;
     }  
  }
```
We will need to add *"no-core-isolation"* knob to have the access-switches continue forwarding even if the Access_cross_link BGP session goes down. For more information on the knob, check this link

[EVPN Core Isolation](https://www.juniper.net/documentation/us/en/software/junos/evpn-vxlan/topics/concept/evpn-vxlan-core-isolation-disabling.html)

    Note: We can still debate the need for no-core-isolation knob in this scenario if aggregate bundle is used to have link-redudanct on Access_cross_link

## 6. BGP Configuration

BGP Configuration will also be same as in current model for EVPN Leafs.

We will build an Underlay session using the link addresses. This BGP session will exchange access-switch lo0 IP address reachability. Overlay BGP session will be used to exchange ESI-LAG related evpn signaling.

```diff
 protocols {  
     bgp {  
         log-updown;  
         graceful-restart;  
         multipath;  
         group l3clos-l {  
             type external;  
             multipath {  
                 multiple-as;  
             }  
             export BGP-AOS-Policy;  
             neighbor 192.168.0.6 {  
                 description "facing_col-2leaf-2access-001-leaf2";  
                 local-address 192.168.0.7;  
                 peer-as 64521;  
                 family inet {  
                     unicast;  
                 }  
             }  
         }  
         group l3clos-l-evpn {  
             type external;  
             multipath {  
                 multiple-as;  
             }  
             multihop {  
                 no-nexthop-change;  
                 ttl 1;  
             }  
             family evpn {  
                 signaling {  
                     loops 2;  
                 }  
             }  
             neighbor 10.0.0.28 {  
                 description "facing_col-2leaf-2access-001-leaf2-evpn-overlay";  
                 local-address 10.0.0.27;  
                 peer-as 64521;  
                 family evpn {  
                     signaling;  
                 }  
             }  
         }  
     }  
 }  
 policy-options {  
     policy-statement AllPodNetworks {  
         term AllPodNetworks-10 {  
             from {  
                 family inet;  
                 protocol direct;  
             }  
             then accept;  
         }  
         term AllPodNetworks-100 {  
             then reject;  
         }  
     }  
     policy-statement BGP-AOS-Policy {  
         term BGP-AOS-Policy-10 {  
             from {  
                 policy AllPodNetworks;  
             }  
             then accept;  
         }  
         term BGP-AOS-Policy-100 {  
             then reject;  
         }  
     }  
     policy-statement PFE-LB {  
         then {  
             load-balance per-packet;  
         }  
     }  
 }  
```  